package de.grogra.fastrakReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;


import de.grogra.graph.impl.Node;

public class Fastrak_Import {
	public Fastrak_Import(File f, Node root) {
		XMLReader xmlReader = null;
		try {
			xmlReader = XMLReaderFactory.createXMLReader();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    FileReader reader = null;
		try {
			reader = new FileReader(f.getAbsolutePath());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    InputSource inputSource = new InputSource(reader);
	    XContentHandler xc=new XContentHandler();
	    xmlReader.setContentHandler(xc);
	    try {
			xmlReader.parse(inputSource);
		} catch (IOException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    for(int i =0; i< xc.getAllRoots().size(); i++) {
	    	Branch part = xc.getAllRoots().get(i);
	    	if(part.parendId!=-1) {
	    		getParentPoint(xc.getAllRoots().get(part.parendId),i,part.points.get(0).getX(),part.points.get(0).getY(),part.points.get(0).getZ());
	    	}
	    }
	    
	    
	    Node old = root;
	    for(Branch part : xc.getAllRoots()) {
	    	if(part.parendId==-1) {
	    			part.createGraph(old,xc.getAllRoots(),0,0,0);
	    	}
	    }
	}
	double getDistance(double startx, double starty, double startz, double d, double e, double f) {
		
		return Math.sqrt(Math.sqrt(Math.pow(startx-d,2)+Math.pow(startx-d,2))+Math.pow(startx-d,2));
	}
	
	void getParentPoint(Branch parent, int kiddoId, double startx,double starty,double startz) {
		int minp=1;
		double tmp=0;
		tmp=getDistance(startx, starty, startz,parent.points.get(0).getX(),parent.points.get(0).getY(),parent.points.get(0).getZ());
		double min=tmp;
		for(int i=1; i<parent.points.size()-1;i++) {
			tmp=getDistance(startx, starty, startz,parent.points.get(i).getX(),parent.points.get(i).getY(),parent.points.get(i).getZ());		
			
			if(tmp<min) {
				minp=i;
				min=tmp;
			}
		}
		Point x=parent.points.get(minp);
		x.addChild(kiddoId);
		parent.points.set(minp, x);
	
	}
}
