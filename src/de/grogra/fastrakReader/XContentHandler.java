package de.grogra.fastrakReader;

import java.util.ArrayList;


import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import org.xml.sax.helpers.DefaultHandler;

public class XContentHandler  extends DefaultHandler {
	private ArrayList<Branch> allRoots = new ArrayList<Branch>();
	private String currentValue;
	int rootid=-1;
	int shootId=-1;
	int currentId=-1;
	float x, y, z, diameter = 1000000000;
	
	Branch last;
	
	//getter
	public ArrayList<Branch> getAllRoots(){
			return allRoots;
	}
	
//implementing ContentHandler
	public void setDocumentLocator(Locator locator) {
		// TODO Auto-generated method stub
		
	}

	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void endPrefixMapping(String prefix) throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(localName.equals("Topo")) {
			if(diameter != 1000000000) {
				allRoots.get(currentId).addPoint(x,y,z);
				allRoots.get(currentId).addDiameter(diameter);	
			}
			if(currentValue.contains("*")){
				Branch r = new Branch(0);
				if(currentId==-1) {
					r = new Branch(-1);				
				}
				/*if(currentId!=-1){
					allRoots.get(currentId);
				}*/
			//	last = r;
				allRoots.add(r);
				currentId=allRoots.size()-1;	
				allRoots.get(currentId).setRootId(currentId);
			}
		}
		if(localName.equals("X")) {x = Float.parseFloat(currentValue.replace(",", "."));}
		if(localName.equals("Y")) {y = Float.parseFloat(currentValue.replace(",", "."));}
		if(localName.equals("Z")) {z = Float.parseFloat(currentValue.replace(",", "."));}
		if(localName.equals("diameter")) {diameter = Float.parseFloat(currentValue.replace(",", "."));}

	}
	

	public void characters(char[] ch, int start, int length)
			throws SAXException {
		 currentValue = new String(ch, start, length);
		
	}

	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void processingInstruction(String target, String data)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void skippedEntity(String name) throws SAXException {
		// TODO Auto-generated method stub
		
	}
}
