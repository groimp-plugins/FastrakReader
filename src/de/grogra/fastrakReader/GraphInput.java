package de.grogra.fastrakReader;

import java.io.File;
import java.io.IOException;

import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;

public class GraphInput extends FilterBase implements ObjectSource {

	
	public GraphInput(FilterItem item, FilterSource source)  {
		
		super(item, source);
		setFlavor(IOFlavor.valueOf(Node.class));	
		
	}

	@Override
	public Object getObject() throws IOException {
		File f = ((FileSource) source).getInputFile ();	
		Node tmpRoot=new Node();
		new Fastrak_Import(f,tmpRoot);
		return tmpRoot;
	}
}